dnf repoquery --userinstalled
 (and then remove anything that is not in the package list: typically the initial installation packages)

sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

sudo dnf config-manager --enable fedora-cisco-openh264

sudo dnf groupupdate multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin

 (Not so sure about these two)
sudo dnf swap mesa-va-drivers mesa-va-drivers-freeworld
sudo dnf swap mesa-vdpau-drivers mesa-vdpau-drivers-freeworld

sudo dnf install rpmfusion-nonfree-release-tainted
sudo dnf --repo=rpmfusion-nonfree-tainted install "*-firmware"

sudo fwupdmgr get-devices
sudo fwupdmgr refresh
sudo fwupdmgr get-updates
sudo fwupdmgr update

sudo python -m ensurepip
python -m ensurepip
sudo pip install -U pip
pip install -U pip
sudo pip install django matplotlib numpy pandas pillow pytest requests scipy urllib3

https://www.reaper.fm/download.php

https://openscad.org/downloads.html  (Development Snapshot, contains various performance improvements)
https://github.com/openscad/MCAD/tree/dev
 Use https://packages.fedoraproject.org/pkgs/openscad/ to recreate file tree, including application shortcut

Replace default apps?
- Mail
- Calendar
- Music player
- Video player
- Image viewer
- Text Editor
- Virtual Machines

Remove (--noautoremove)
======
*backgrounds*
gedit
gnome-abrt
gnome-boxes
gnome-calculator
gnome-characters
gnome-clocks
gnome-connections  (with deps)
gnome-contacts
gnome-initial-setup
gnome-logs
gnome-maps  (with deps)
gnome-photos
gnome-text-editor  (with deps)
gnome-tour
intel-gpu-firmware  (AMD)
libreoffice*  (with deps, probably)
nouveau-firmware  (AMD)
nvidia-gpu-firmware  (AMD)

 If anything was forgotten:
dnf repoquery --installed | grep "^gnome"

Configure:
- browser (firefox and/or chrome)
  - password manager
  - ublock origin
  - gnome extension extension
  - bookmarks
    - "New HW" https://gearspace.com/board/forum9/prefix-24/?sort=dateline
    - "Quick Note" (keyword 'note') `data:text/html, <html contenteditable><head><title>::Quick Notes::</title><script>window.onbeforeunload=function(e){return e.returnValue=true};</script></head><body><ol><li></li></ol></body></html>`
- gnome settings
- gnome tweaks
- gnome extensions
  - vitals
  - just perfection
- dconf (optionally)
- dotfiles
  - alacritty
  - ripgrep
  - vim
  - zsh

 When all is said and done (and every so often):
sudo dracut -fv
