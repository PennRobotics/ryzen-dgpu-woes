# Fix Errors in Linux Workstation

_ref: http://www.brendangregg.com_

## USB

- `lsusb`
- `rmmod usbhid ; sleep 1; modprobe usbhid`


## Errors / Logs

### journalctl

- `journalctl -k -p crit`


## Storage

- `fsck` (Boot using Live Image)
