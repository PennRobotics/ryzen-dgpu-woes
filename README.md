Custom Ryzen Tower Woes
=======================

## Outcome

The parts arrived and I put them together. After an uncomfortably long black screen, I was greeted by the ASRock logo and then the Fedora Live image boot menu. I changed BIOS settings to what felt right, installed Fedora, had a few crashes, updated firmware and software, and then watched some video and played SuperTuxKart. In short, it was mostly painless and problem-free, and assembly was quick and easy. 

### Lessons

The original power supply was too loud because at low speeds, the fan motor would softly click. Its replacement, a Seasonic Prime, is quiet with the fan on and has a silent mode where the fan will turn completely off.

I also moved the case fans to the motherboard pins. There was not a great reason to keep the fans on a panel-mounted switch.

My original notes are listed:

* A Noctua CPU cooler comes with thermal paste, so do not buy a standalone tube of NT-H1.
* The motherboard comes with M2 coolers, so you will not need to buy a standalone M2 cooler.
* Even with decent documentation, it is difficult to understand which PCIE slot is appropriate for a PCIE 4.0 x16 (or is it x8?) GPU. Since PCIE is backwards compatible, I just put it in the PCIE 5.0 slot and didn't give it any more thought.
* This still ended up louder than I would have liked, but it is probably the quietest tower I've had (and quieter than some laptops). I'll probably move the chassis fans from the case switch (which is always on) to the motherboard because the CPU runs quite cold (40 to 45 deg. C with the fan at 450 RPM) when I'm not doing anything intensive. I'll also need to see if there's a way to get the PSU fan to stop. From the description of the PSU, it should either run with the fan off at half load or slow enough to be inaudible farther than one meter away. It's not clear if the switch on the GPU is "mining mode" or "quiet mode" (or if those mean the same thing), but there are settings in BIOS to limit power consumption. I'll need to see how much compute power is needed while recording music and then check if there are ways to get the loudest fans to turn slower.
* Linux support for each component seems great so far. S/PDIF and jack detection works. I haven't checked if ECC is functional yet but I enabled it in BIOS. You have to add codecs, but then the GPU works perfectly fine for gaming and videos.

#### July 2024, AMD GPU overheating during suspend

Twice now, the computer has gone into suspend and then the GPU fan starts spinning at 100%. This is with some kernel 6.9.x. The hope is that all of these current updates to **mesa** and the kernel will bring a fix, but now AMD's driver bullshit is reaching the desktop, and that gets me irritated.

### Checklist

- [ ] check if anything needs to be kept from `dnf autoremove`
- [ ] check logs, make changes until major errors are gone
- [ ] read each BIOS setting, make changes
- [x] verify ECC is working
    - [ ] is it the real ECC or the DDR5 on-chip stuff?
- [x] move chassis fans to individual connectors
- [ ] find a USB-C 3.2 Gen 2 connector, or whatever that thing is called. Maybe the fan switch could be removed and replaced with a USB-C front panel port?
- [ ] PC speaker? Other mobo connectors?
- [ ] Secure boot or vanilla kernel?
- [ ] Read CPU fan speed from Linux
- [ ] Set up a sensitive microphone to benchmark fan noise. First record with laptop and/or field recorder (no fan noise) and then various tower changes: distance to mic, floor material, location in room/orientation, disabling fans or holding them motionless, GPU switch, CPU fan "quiet" cable extender. Add mixers (standalone and connected to tower)
- [ ] While we're testing audio, see if there is a different noise floor between the front 3.5mm jacks and the rear panel
- [ ] Which GPU switch setting is quieter? Does it make a difference?
- [ ] Can the blinking power LED during sleep be modified or disabled? (ideally without changing the normal "power on" state/brightness, but I'd rather put a dimming sticker/tape on the button than disable the LED completely)
- [ ] Connect all peripherals
    - [ ] audio interfaces
        - [ ] Shure
        - [ ] Allen and Heath
        - [ ] Zoom
    - [ ] gamepads
        - [ ] wired
        - [ ] wireless
    - [ ] Bluetooth mouse
        - [ ] possible to wake-on-activity? side effects?
    - [ ] optical audio
    - [ ] cameras
        - [ ] Logitech
        - [ ] Sony Alpha
    - [ ] external SSD
    - [ ] Brother
        - [ ] print
        - [ ] scan
    - [ ] 3.5 mic
    - [ ] plugs for unused connectors
    - [ ] 10bpp color?
    - [ ] under-keyboard switches?
    - [ ] how to handle SD cards? external adapter? case mod? PCIe reader?
    - [ ] ??
- [ ] Fix up customizations (e.g. color scheme) in the AppImage version of OpenSCAD, if possible
- [ ] Add browser bookmarks (including "Quick Note", "New HW", etc.)

-----

## Tangent: GitLab sucks

First off, special shoutout to GitLab for having the shittiest editor and file management interface that I have the displeasure of regularly using. No, I do not want your cloud version of Visual Studio. No, I do not want keyword autocomplete in Markdown files. I want to be able to single-file-edit something that is empty and has a non-text extension. Highlighting and smart spaces should just work correctly. (First-level headings are a perfect example. The alternative method of setting the title/first-level heading&mdash;a long line of equals signs&mdash;does not highlight the line above.) I have a handful of other examples, but that distracts from the original point of potentially complaining about the system I have planned and will build:

-----

## Tangent: PCs suck

As my last three PCs (specifically, computers that run Windows or Linux as an operating system) have been epic pieces of shit, I wanted to create this document to store any complaints I develop while building and using a custom PC. This is possibly the last straw before I permanently switch to a tablet as my development PC and daily driver. PCs have really turned awful in the last half decade&hellip; coincidentally right around the time Microsoft acquired GitHub, started aggressively pushing Windows Modern Standby, and switched their focus toward Windows 11.

**The Lenovo ThinkPad Z13 is a fucking awful laptop. Do Not Buy.**

**The Intel NUC 11 Enthusiast Kit is a fucking awful SFF desktop. Do Not Buy.**

**Clevo gaming laptops are fucking awful. Do Not Buy.**

You see where this leaves me? Intel is unable to make a computer to support their own processor whether or not the thing runs Windows or Intel Clear Linux OS. (Tangentially, this OS follows the open-source tradition of having dumb names for Linux-kernel operating systems.) They have all the datasheets they need and still have buggy, terrible hardware, firmware, and software. A top-of-the-line AMD computer from one of the largest manufacturers that runs an AMD GPU is equally garbage running either Windows or one of the popular Linux distros. The ThinkPad line is supposed to be bulletproof&mdash;both in mechanical durability as well as its software support. What. A. Fucking. Joke!

-----

## Tangent: Sometimes Linux is not amazing

When starting up this PC, I had a few crashes. I updated my software and then still had one or two more **abrt** events because of crashes. (In the second case, I closed SuperTuxKart, which looked normal to me but was reported as an application crash.) Generating backtraces takes such a long time! Sometimes, you generate a backtrace and then the bug report tool indicates there isn't useful information in your crash log. Other times, it's obvious the problem will only happen once and was just poorly programmed (such as enumerating the available printers in the "Color management" tab of _Settings_ while the printer driver is still being installed, which causes a fault) and it will take far longer to prepare a bug report or fix than to just ignore. I think what I need to do is just figure out what the team does that is on the receiving end of these bug reports, and do that stuff myself. Thus, I disabled automatic crash reporting and will try to devote a bit of time to creating my own postmortems and preventing issues.

I get that the reasonably fresh kernel and a close-to-upstream distro will have a handful of bugs, and some of these will be critical. Fedora is also extremely popular, and I'm not running a rolling release. I don't install kernel-next or mainline. I would expect that GTK 4 and the basic GNOME apps should be unit tested to the point where a printer enumeration error or laggy Bluetooth toggle simply _cannot_ happen.

Then again, I look at the bugs that are fresh to Windows 11 and am shocked at the state of modern operating systems. I still think if you want reliability, buy an Android tablet and figure out how to get your workflow to depend on that tablet.

-----

## Custom PC

I wanted to give this repository a more memorable name, but I also did not want to define this build by one of its components, so I purposefully am not calling it the _AMD Build_ or _ASRock Radeon Build_ or _Linux Ryzen Build_ or anything else like that, because its success or failure would likely be a combination of factors. Besides, then it joins the army of custom Linux/Ryzen/Radeon/ASRock builds that have gone before and will continue after. This one is one-of-a-kind. This one is MY lousy machine, so when it inevitably disappoints, that will be the result of all of MY choices.

| Component type | Brand | Exact selection | Detailed description | Power usage |
| -------------- | ----- | --------------- | -------------------- | ----------- |
| Motherboard    | ASRock | B650E Taichi Lite   | E-ATX dual-PCIe x16, 4x DDR5, Realtek ALC4082, BT 5.3, Wi-Fi 6E, etc. | 50 W, maybe? |
| RAM            | Kingston | Server Premier DDR5-4800 32 GB   | single stick, ECC unbuffered, 1.1V, Hynix M die, 40-39-39 | a few watts |
| CPU            | AMD | Ryzen 5 7500F   | no iGPU, 6 cores, 32 MB cache | 65 W |
| CPU heatsink   | Noctua | NH-U12S SE-AM4   | 19 to 22 dB, 50 cfm, 4-pin PWM | 1 W |
| Thermal paste  | Noctua | NT-H1   |  3.5g, AM5 Edition | N/A |
| GPU            | XFX | Radeon RX 7600 Speedster   | aka SWFT210 | 165 W |
| SSD            | Corsair | MP700 1 TB   | PCIe 5.0 x4 NVMe M.2 2280 | 10 W |
| SSD heatsink   | be quiet! | MC1 PRO   | passive block, M.2 2280 | N/A |
| Case           | be quiet! | Silent Base 601 Black   | E-ATX, large, insulated | minimal |
| PSU            | <s>be quiet!</s> | <s>Straight Power 12 850W</s>   | <s>ATX3.0, 80 PLUS Platinum, 8 to 22 dB, 94% efficient</s> | <s>50 W</s> |
| PSU            | Seasonic | Prime TX 750W   | ATX 12V, 80 PLUS Titanium, **0** to 17 dB, 94% efficient | 50 W |

Overall, the power consumption is estimated to be around 350 watts during moderate-to-heavy usage while the PSU is built for a peak of <s>850</s> 750 watts. Below about 300 watts, the hybrid mode button on the power supply will allow it to remain silent. The fan will automatically turn on if the supply starts getting hot.

### Decisions

- **CPU**: wanted AMD because Intel has disappointed me more often, and something with no built-in GPU because dual graphics has been a plague since Optimus or whatever the hell that Nvidia hybrid was called. Beyond that, I have no urgent need for Threadripper but would have room to upgrade later, if needed.
- **Motherboard**: wanted something to support the AM5 Ryzen plus true ECC memory (because who actually knows if stability issues come from faulty RAM?), this was chosen from ASRocks assortment because it has the potentially least crappy audio codec. Still, Realtek and Linux go together like vinegar and my tongue. Yuck!
- **PSU**, **Cooling**, and **Case**: _be quiet!_ and _Noctua_ have a reputation for quiet, reliable products. Easy as that!
- **GPU**: wanted something AMD-based for Linux with as much hardware codec support as possible, quiet running, nothing too extreme because I am not looking to game, but something that will not reach rocket launch volumes while watching videos on Youtube. After all, this will be a PC used for recording music.
- **SSD**: something to take advantage of PCIE 5.0, and Corsair should be reliable enough.
